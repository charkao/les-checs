
#include <iostream>         // cin, cout, ...
// Programmes locaux
#include "u1-interface.h"
#include "u2-dessin.h"
#include "u3-callbacks.h"
#include "u4-fonctions.h"

// Declaration pour utiliser iostream
using namespace std;

// Definition des donnees de l'interface - structure globale de variables
struct Interface gInterface ;

// CreerInterface
void CreerInterface()
{
    // Creation de la fenetre principale
    gInterface.Fenetre = new Fl_Double_Window(1150,900);
    gInterface.Fenetre->label("Projet Type") ;
    gInterface.Fenetre->begin() ;

    // Creation de la zone de dessin
    gInterface.ZoneDessin = new DrawingArea(X_ZONE,Y_ZONE,L_ZONE,H_ZONE);
    gInterface.ZoneDessin->draw_callback( ZoneDessinDessinerCB, NULL ) ;
    gInterface.ZoneDessin->mouse_callback( ZoneDessinSourisCB, NULL ) ;
    gInterface.ZoneDessin->keyboard_callback( ZoneDessinClavierCB, NULL ) ;

    // Creation du bouton Quitter
    gInterface.BoutonQuitter = new Fl_Button(20, 840, 100, 20, "Quitter") ;
    gInterface.BoutonQuitter->callback( BoutonQuitterCB, NULL ) ;

    // Creation du bouton Jouer
    gInterface.BoutonJouer = new Fl_Button(960, 230, 100, 20, "Jouer") ;
    gInterface.BoutonJouer->callback( BoutonJouerCB, NULL ) ;

    // Creation du bouton Pause
    gInterface.BoutonPause = new Fl_Button(960, 270, 100, 20, "Pause") ;
    gInterface.BoutonPause->callback( BoutonPauseCB, NULL ) ;

    // Creation du bouton Annuler
    gInterface.BoutonAnnuler = new Fl_Button(960, 310, 100, 20, "Annuler Coup") ;
    gInterface.BoutonAnnuler->callback( BoutonAnnulerCB, NULL ) ;


    // Creation du bouton radio
    /* Correctif PK : les coordonnees du groupe de boutons et des boutons radios */
    //gInterface.GroupeBoutonsRadios = new Fl_Group(870, 80, 100, 20, "Mode de jeu") ;
    gInterface.GroupeBoutonsRadios = new Fl_Group(920, 60, 180, 60, "Mode de jeu") ;
    //gInterface.GroupeBoutonsRadios->align( FL_ALIGN_LEFT ) ;
    gInterface.GroupeBoutonsRadios->begin() ;
    gInterface.BoutonRadio1 = new Fl_Round_Button(920, 60, 180, 20, "Ordinateur vs Ordinateur") ;
    gInterface.BoutonRadio1->type(FL_RADIO_BUTTON);
    gInterface.BoutonRadio1->callback(BoutonRadioCB, NULL ) ;
    gInterface.BoutonRadio2 = new Fl_Round_Button(920, 80, 180, 20, "Ordinateur vs Humain") ;
    gInterface.BoutonRadio2->type(FL_RADIO_BUTTON);
    gInterface.BoutonRadio2->callback(BoutonRadioCB, NULL ) ;
    gInterface.BoutonRadio3 = new Fl_Round_Button(920, 100, 180, 20, "Humain vs Humain") ;
    /* Fin Correctif PK */
    gInterface.BoutonRadio3->type(FL_RADIO_BUTTON);
    gInterface.BoutonRadio3->callback(BoutonRadioCB, NULL ) ;
    gInterface.GroupeBoutonsRadios->end() ;

    // Creation du bouton curseur
    gInterface.Curseur = new Fl_Value_Slider( 1010, 150, 100, 20, "Niveau de l'ordinateur" ) ;
    gInterface.Curseur->type( FL_HORIZONTAL ) ;
    gInterface.Curseur->align( FL_ALIGN_LEFT ) ;
    gInterface.Curseur->callback( CurseurCB, NULL ) ;
    gInterface.Curseur->bounds( 1,5 ) ;
    gInterface.Curseur->precision( 0 ) ;

    /* Correctif PK : ajout champ d'information */
    gInterface.JoueurEnCours = new Fl_Output(1010, 180, 60, 20, "Joueur courant : ") ;

    gInterface.ScoreB = new Fl_Value_Output(960, 370, 100, 20, "Score joueur blanc") ;
    gInterface.ScoreN = new Fl_Value_Output(960, 410, 100, 20, "Score joueur noir") ;

    // Affichage de la fenetre
    gInterface.Fenetre->end();
    gInterface.Fenetre->show();

}

/* Correctif PK : actualisation automatique du champ JoueurEnCours sur l'interface */
void ActualiserInterface()
{
    if(gDonnees.JeuEnCours->Joueur == BLANC){
        gInterface.JoueurEnCours->value("BLANC");
    } else {
        gInterface.JoueurEnCours->value("NOIR");
    }
    gInterface.ScoreB->value(Score(gDonnees.JeuEnCours->Plateau, BLANC));
    gInterface.ScoreN->value(Score(gDonnees.JeuEnCours->Plateau, NOIR));
    gInterface.Curseur->value(gDonnees.Profondeur);
    if ((gDonnees.EstHumainBlanc)&&(gDonnees.EstHumainNoir)){
        gInterface.BoutonRadio3->value(1) ;
    }
    else  if ((gDonnees.EstHumainBlanc==0)&&(gDonnees.EstHumainNoir)){
        gInterface.BoutonRadio2->value(1) ;
    }
    else  if ((gDonnees.EstHumainBlanc==0)&&(gDonnees.EstHumainNoir==0)){
        gInterface.BoutonRadio1->value(1) ;
    }
    else  if ((gDonnees.EstHumainBlanc)&&(gDonnees.EstHumainNoir==0)){
        gInterface.BoutonRadio2->value(1) ;
  }
}
