/* Othello u4_fonctions.h - proposition initiale de structure de donnees et de fonctions (P. Kocelniak)*/
/* NOTA BENE : A adapter pour les echecs */

// u4-fonctions.h
// Sentinelle d'inclusion
#ifndef _u4_fonctions_h
#define _u4_fonctions_h

// Definition des constantes
#define DUREE_CYCLE 1    // 0.500 secondes, depend du materiel utilise

// Declaration des donnees du projet

/*
	Structures de donnees du projet
*/
/* L'enumeration Couleur sert a la fois : 
   - pour designer le type de chaque cellule du plateau de jeu
   - pour la couleur du joueur en cours (2 valeurs possibles dans ce cas : BLANC ou NOIR
*/
enum Couleur{BLANC,NOIR,VIDE}; 	// BLANC : 0, NOIR : 1, VIDE : 2

// Structure pour la liste des coups possibles (liste doublement chainee circulaire)
struct CoupPossible
{
   int L1; // Ligne de la cellule jouee
   int C1; // Colonne de la cellule jouee
   long Evaluation; 				// Evaluation du coup, vue du point de vue du Joueur de JeuEnCours
   struct CoupPossible *Suivant;	// Pointeur vers le coup possible suivant
   struct CoupPossible *Precedent;	// Pointeur vers le coup possible precedent
};

struct Jeu
{
    enum Couleur Joueur;			// Couleur du joueur en cours
    enum Couleur **Plateau;			// Plateau de jeu a allouer dynamiquement en 2D contigu "encore plus fort"
};

// Structure globale pour les variables fonctionnelles
struct Donnees
{
    // Variables du projet a definir ici
    struct Jeu *JeuEnCours;				// Structure de jeu courante
	int NbLig;							// Nombre de lignes du plateau
	int NbCol;							// Nombre de colonnes du plateau
    bool EstHumainBlanc;				// Ordi ou humain pour le joueur 1 Blanc
    bool EstHumainNoir;					// Ordi ou humain pour le joueur 2 Noir
    int Profondeur;						// Profondeur algorithme AlphaBeta
	int NbClics;    					// Mode joueur Humain : Nb de clics effectue, 0, 1 (L1,C1 cellule jouee)
	int ClicL1, ClicC1;					// Mode Joueur Humain : Coordonnees clic 1
};

// Variable globale, gDonnees est accessible depuis toutes les fonctions de tous les modules, u1... a u4...
extern struct Donnees gDonnees;

/*
	Prototypes des fonctions a creer dans u4-fonctions.cpp
*/
/* Allocation dynamique par la methode 2D contigue "encore plus fort" */
enum Couleur** AllouerPlateau(int NbLig, int NbCol);
/* Initialisation des donnees initiales : jeu, NbLig, NbCol, EstHumainBlanc, EstHumainNoir, Profondeur, NbClics, ClicL1, ClicC1,... */
/* Cette fonction utilisera AllouerPlateau entre autres */
void InitialiserDonnees();

/*
	Gestion de la liste des coups possibles. Choix d'une liste doublement chainee circulaire.
*/
/* Cette fonction retourne true si le coup joue de L1,C1 en L2,C2 est valable, false sinon */
bool CoupJouable(enum Couleur **Plateau, int NbLig, int NbCol, int L1, int C1, enum Couleur Joueur);
/* Initialisation de la liste des coups possibles :
   retourne un pointeur sur une liste initiale constituee d'une seule premiere cellule de type struct CoupPossible
   Pour cette premiere cellule "vide":
   - L1, C1 seront initialises avec la valeur -1 signifiant que la cellule est vide pour le moment
   - Les pointeurs Suivant et Precedent pointeront sur la cellule elle meme (principe d'une liste doublement chainee circulaire
*/
struct CoupPossible* CreerListesCoupsPossiblesInitiale();
/* Cette fonction insere une nouvelle cellule a la fin de la liste des coups possibles Liste avec les valeurs des parametres L1, C1, L2, C2 */
void AjouterListeCoupsPossibles(struct CoupPossible* Liste, int L1, int C1);
/* Cette fonction cree la liste complete des coups possibles pour le Plateau passe en parametre et le joueur en cours
   Principes :
   - Appel a CreerListesCoupsPossiblesInitiale
   - Methode "force brute" : on teste via des boucles tous les coups L1, C1 dans le Plateau
   - Pour chaque couple, la fonction CoupJouable est utilisee
   - Si CoupJouable retourne true, on ajoute a la liste des coups possibles via la fonction AjouterListeCoupsPossibles
*/
struct CoupPossible* CreerListeCoupsPossibles(enum Couleur **Plateau, int NbLig, int NbCol, enum Couleur Joueur);
/* Liberation de toute la memoire allouee dynamiquement pour la liste Liste */
void Liberer(struct CoupPossible *Liste);

/*
	Jeu ordinateur
*/
/* Cette fonction joue le coup L1,C1 dans Plateau et modifie donc les valeurs des cellules de Plateau */
void Jouer(enum Couleur **Plateau, int NbLig, int NbCol, int L1, int C1);
/* Cette fonction cree la liste des coups possibles pour la situation de jeu passee en parametres
   Elle cherche le meilleur coup a jouer et le joue finalement en modifiant les donnees du jeu courant :
   - Appel a CreerListeCoupsPossibles
   - Parcours de la liste des coups possibles et calcul de Evaluation pour chaque coup via un appel a AlphaBetaAmi
   - Choix du meilleur coup (Evaluation la plus grande)
   - On joue le meilleur coup et on change les donnees du jeu pour passer au joueur suivant
*/
void JouerMeilleurCoup(struct Jeu *Jeu, int NbLig, int NbCol);

/*
	Fonctions AlphaBeta : mise en oeuvre de l'algorithme AlphaBeta pour retourner l'Evaluation d'un coup de la liste des coups possibles
	Cf. fiche AlphaBeta et pseudo code fourni
	Sorte de recursivite avec 2 fonctions :
	- AlphaBetaAmi fait des appels a AlphaBetaEnnemi
	- AlphaBetaEnnemi fait des appels a AlphaBetaAmi
*/
long AlphaBetaAmi(enum Couleur **Plateau, int NbLig, int NbCol, long Alpha, long Beta, int Profondeur, enum Couleur JoueurEnCours);
long AlphaBetaEnnemi(enum Couleur **Plateau, int NbLig, int NbCol, long Alpha, long Beta, int Profondeur, enum Couleur JoueurEnCours);
/* Retourne l'evaluation du plateau de jeu Plateau vue du joueur en cours */
long Evaluer(enum Couleur **Plateau, int NbLig, int NbCol, enum Couleur JoueurEnCours);

/*
	Autres fonctions utilitaires
*/
/* Retourne le nombre de coups possibles de la liste */
int NbCoupsPossibles(struct CoupPossible* Liste);
/* Conditions de fin du jeu. Retourne true si le jeu est termine, faux sinon */
bool FinDuJeu(enum Couleur **Plateau, int NbLig, int NbCol);
/* Fonctions de trace dans un fichier en mode texte : cela servira a des verifications */
FILE* OuvrirFichierTrace(char *Fichier);	// Ouverture en mode texte et ecriture d'un fichier de trace dont le nom est passe en parametre
void TraceExporterListeCoupsPossibles (FILE *f, struct CoupPossible* Liste);	// Ecrire dans le flux de fichier f la liste des coups possibles
void TraceExporterDonnees (FILE *f, struct Donnees *Donnees);	// Ecrire dans le flux de fichier f les differentes variables de la structure Donnees
void FermerFichierTrace(FILE *f);	// Fermeture du fichier de trace

// Utilitaires fournis par le kitc-phelma
void JouerSon(char *) ;         // Jouer un son
void Attente(double Seconds);   // Procedure d'attente

#endif // _u4_fonctions_h