// u3-callbacks.h
// Sentinelle d'inclusion
#ifndef _u3_callbacks_h
#define _u3_callbacks_h

// Declarations externes
#include <FL/Fl_Widget.H>

// Declaration des sous-programmes
void TraiterCycleCB() ;
void ZoneDessinSourisCB( Fl_Widget* widget, void* data ) ;
void ZoneDessinClavierCB( Fl_Widget* widget, void* data ) ;
void BoutonQuitterCB( Fl_Widget* w, void* data ) ;
void BoutonRadioCB(Fl_Widget* w, void* data);
void CurseurCB ( Fl_Widget* w, void* data ) ;
void BoutonJouerCB ( Fl_Widget* w, void* data );
void BoutonPasserCB ( Fl_Widget* w, void* data );
void BoutonAnnulerCB ( Fl_Widget* w, void* data );


#endif // _u3_callbacks_h
