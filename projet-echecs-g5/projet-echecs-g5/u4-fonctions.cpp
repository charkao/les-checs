// u4-fonctions.cpp
// Declarations externes - inclusion des fichiers d'entete
// Librairies standards
#include <iostream>     // cout, cin, ...
#include <stdlib.h>     // exit, rand
#include <time.h>       // time
#include <string.h>     // strcpy
#include <stdio.h>
#include <math.h>

// Librairie fmod pour le son
#include <api/inc/fmod.h>
#include <api/inc/fmod_errors.h>
// Programmes locaux
#include "u1-interface.h"
#include "u4-fonctions.h"

// Declaration pour utiliser iostream
using namespace std;

// Definition des donnees fonctionnelles du projet - structure globale de variables
struct Donnees gDonnees;

// Initialiser
void InitialiserDonnees()
{   int i, j;
    // On initialise le generateur de nombres aleatoires
    srand(time(NULL));
    gDonnees.JeuEnCours=(struct Jeu*) calloc(1, sizeof(struct Jeu));
    gDonnees.JeuEnCours->Joueur=BLANC;
    gDonnees.EstHumainBlanc=false;
    gDonnees.EstHumainBlanc=false;
    gDonnees.Profondeur=1;
    gDonnees.NbClics=0;
    gDonnees.ClicL1=0;
    gDonnees.ClicC1=0;
    gDonnees.ClicL2=0;
    gDonnees.ClicC2=0;
    gDonnees.Alpha=-1000000000;
    gDonnees.Beta=1000000000;

    gDonnees.JeuEnCours->Plateau[0][0].Type=TOUR;
    gDonnees.JeuEnCours->Plateau[0][1].Type=CAVALIER;
    gDonnees.JeuEnCours->Plateau[0][2].Type=FOU;
    gDonnees.JeuEnCours->Plateau[0][3].Type=REINE;
    gDonnees.JeuEnCours->Plateau[0][4].Type=ROI;
    gDonnees.JeuEnCours->Plateau[0][5].Type=FOU;
    gDonnees.JeuEnCours->Plateau[0][6].Type=CAVALIER;
    gDonnees.JeuEnCours->Plateau[0][7].Type=TOUR;
    for (j=0; j<8; j++){
      gDonnees.JeuEnCours->Plateau[1][j].Type=PION;
    }
    for (i=0; i<2; i++){
      for (j=0; j<8; j++){
        gDonnees.JeuEnCours->Plateau[i][j].Couleur=NOIR;
      }}

    for (i=2; i<6; i++){
      for (j=0; j<8; j++){
          gDonnees.JeuEnCours->Plateau[i][j].Type=AUCUNE;
          gDonnees.JeuEnCours->Plateau[i][j].Couleur=VIDE;
        }}

    for (j=0; j<8; j++){
        gDonnees.JeuEnCours->Plateau[6][j].Type=PION;
  }
    gDonnees.JeuEnCours->Plateau[7][0].Type=TOUR;
    gDonnees.JeuEnCours->Plateau[7][1].Type=CAVALIER;
    gDonnees.JeuEnCours->Plateau[7][2].Type=FOU;
    gDonnees.JeuEnCours->Plateau[7][3].Type=REINE;
    gDonnees.JeuEnCours->Plateau[7][4].Type=ROI;
    gDonnees.JeuEnCours->Plateau[7][5].Type=FOU;
    gDonnees.JeuEnCours->Plateau[7][6].Type=CAVALIER;
    gDonnees.JeuEnCours->Plateau[7][7].Type=TOUR;
    for (i=6; i<8; i++){
      for (j=0; j<8; j++){
        gDonnees.JeuEnCours->Plateau[i][j].Couleur=BLANC;
  }
}
JouerMeilleurCoup(gDonnees.JeuEnCours);
}


// Fonctions utilitaires
bool CoupJouable(struct Piece Plateau[NB][NB], uint8_t L1, uint8_t C1, uint8_t L2, uint8_t C2, enum Couleur Joueur) {
int i;
if((L2<0) || (L2>7)){
  return false;
}
if((C2<0) || (C2>7)){
  return false;
}
if (Joueur != Plateau[L1][C1].Couleur){
  return false;
}
if (Joueur==Plateau[L2][C2].Couleur){
  return false;
}
if (Plateau[L1][C1].Type==PION){
  if( Joueur==BLANC){
    if(((C2==C1-1) || (C2==C1+1)) && (L2==L1-1)){
      if(Plateau[L2][C2].Couleur!=(Joueur || VIDE)){
        return true;
}}
if((C2==C1) && (L2==L1-1) && (Plateau[L2][C2].Couleur==VIDE)){
return true ;
}
return false;
}
else if(((C2==C1-1) || (C2==C1+1)) && (L2==L1+1)){
if(Plateau[L2][C2].Couleur!=(Joueur || VIDE)){
return true;
}
if((C2==C1) && (L2==L1+1) && (Plateau[L2][C2].Couleur==VIDE)){
return true ;
}
return false  ;
}


if (Plateau[L1][C1].Type==CAVALIER){
if(((L2==L1+1) || (L2==L1-1)) && ((C2==C1+2) || (C2==C1-2))){
return true;
}
if (((L2==L1+2) || (L2==L1-2)) && ((C2==C1+1) || (C2==C1-1))){
return true;
}
return false;
}

if (Plateau[L1][C1].Type==TOUR){
if ((L2>L1) && (C2==C1)){
for (i=L1+1;i<L2;i++){
if (Plateau[i][C2].Couleur!=VIDE){
return false;
}
}
return true;
}
if ((L2<L1) && (C2==C1)){
for (i=L2+1;i<L1;i++){
if (Plateau[i][C2].Couleur!=VIDE){
return false;
}
}
return true;
}
if ((C2>C1) && (L1==L2)){
for (i=C1+1;i<C2;i++){
if (Plateau[L2][i].Couleur!=VIDE){
return false;
}
}
return true;
}
if ((C2<C1) && (L1==L2)){
for (i=C2+1;i<C1;i++){
if (Plateau[L2][i].Couleur!=VIDE){
return false;
}
}
return true;
}
return false;
}

if (Plateau[L1][C1].Type==FOU){
      if (((L1+C1)%2==0) && (L2+C2%2!=2)){
return false;
}
if (((L1+C1)%2!=0) && (L2+C2%2==2)){
return false;
}
if((L2<L1) && (C2>C1)){
for(i=1;i<L1-L2;i++){
if(Plateau[L1-i][C1+i].Couleur!=VIDE){
return false;
}
}
}
return true;
}
if((L2<L1) && (C2<C1)){
for(i=1;i<L1-L2;i++){
if(Plateau[L2+i][C2+i].Couleur!=VIDE){
return false;
}
}
return true;
}
if((L2>L1) && (C2<C1)){
for(i=1;i<L2-L1;i++){
if(Plateau[L1+i][C1-i].Couleur!=VIDE){
return false;
}
}
return true;
}
if((L2>L1) && (C2>C1)){
for(i=1;i<L2-L1;i++){
if(Plateau[L1+i][C1+i].Couleur!=VIDE){
return false;
}
}
return true;
}
return false;
}

if (Plateau[L1][C1].Type==REINE){
  if ((L2>L1) && (C2==C1)){
    for (i=L1+1;i<L2;i++){
      if (Plateau[i][C2].Couleur!=VIDE){
        return false;
        }
      }
      return true;
    }
  if ((L2<L1) && (C2==C1)){
    for (i=L2+1;i<L1;i++){
      if (Plateau[i][C2].Couleur!=VIDE){
        return false;
        }
      }
      return true;
    }
    if ((C2>C1) && (L1==L2)){
      for (i=C1+1;i<C2;i++){
        if (Plateau[L2][i].Couleur!=VIDE){
          return false;
          }
        }
        return true;
      }
    if ((C2<C1) && (L1==L2)){
      for (i=C2+1;i<C1;i++){
        if (Plateau[L2][i].Couleur!=VIDE){
          return false;
          }
        }
      return true;
    }
    if (((L1+C1)%2==0) && (L2+C2%2!=2)){
      return false;
    }
    if (((L1+C1)%2!=0) && (L2+C2%2==2)){
      return false;
    }
    if((L2<L1) && (C2>C1)){
      for(i=1;i<L1-L2;i++){
          if(Plateau[L1-i][C1+i].Couleur!=VIDE){
              return false;
          }
        }
      return true;
    }
    if((L2<L1) && (C2<C1)){
      for(i=1;i<L1-L2;i++){
        if(Plateau[L2+i][C2+i].Couleur!=VIDE){
          return false;
        }
      }
      return true;
    }
    if((L2>L1) && (C2<C1)){
      for(i=1;i<L2-L1;i++){
        if(Plateau[L1+i][C1-i].Couleur!=VIDE){
          return false;
        }
      }
      return true;
    }
    if((L2>L1) && (C2>C1)){
      for(i=1;i<L2-L1;i++){
        if(Plateau[L1+i][C1+i].Couleur!=VIDE){
          return false;
        }
      }
      return true;
    }
    return false;
  }

    if (Plateau[L1][C1].Type==ROI){
      if((L2=L1-1) && (C2=C1-1)){
        return true;
      }
      if((L2=L1-1) && (C2=C1)){
        return true;
      }
      if((L2=L1-1) && (C2=C1+1)){
        return true;
      }
      if((L2=L1) && (C2=C1+1)){
        return true;
      }
      if((L2=L1) && (C2=C1-1)){
        return true;
      }
      if((L2=L1+1) && (C2=C1-1)){
        return true;
      }
      if((L2=L1+1) && (C2=C1)){
        return true;
      }
      if((L2=L1+1) && (C2=C1+1)){
        return true;
      }
    return false;
  }

}


struct CoupPossible* CreerListesCoupsPossiblesInitiale(){
  /*initialisation de la liste chainée des coups possibles */
  struct CoupPossible *Liste=NULL;
  Liste=(struct CoupPossible*) calloc(1,sizeof(*Liste));
  Liste->L1=255;
  Liste->C1=255;
  Liste->L2=255;
  Liste->C2=255;
  Liste->Evaluation=0;
  Liste->Suivant=NULL;
  return Liste;
}

void AjouterListeCoupsPossibles(struct CoupPossible* Liste, uint8_t LI1, uint8_t CO1, uint8_t LI2, uint8_t CO2){
  if (Liste->L1==255){
    Liste->L1=LI1;
    Liste->C1=CO1;
    Liste->L2=LI2;
    Liste->C2=CO2;
    }
  else {
    struct CoupPossible* Parcours;
    Parcours = Liste;
    while(Parcours!=NULL){
      Parcours=Parcours->Suivant;
    }
      Parcours=(struct CoupPossible*) calloc(1,sizeof(*Parcours));
      Parcours->L1=LI1;
      Parcours->C1=CO1;
      Parcours->L2=LI2;
      Parcours->C2=CO2;
      Parcours->Suivant=NULL;
  }
}

struct CoupPossible* CreerListeCoupsPossibles(struct Piece Plateau[NB][NB], enum Couleur Joueur){
  struct CoupPossible *Liste;
  Liste=CreerListesCoupsPossiblesInitiale();
  uint8_t i,j,k,l;
  for(i=0;i<8;i++){
    for(j=0;j<8;j++){
      for(k=0;k<8;k++){
        for(l=0;l<8;l++){
          if(CoupJouable(Plateau, i,j,k,l,Joueur)==true){
            AjouterListeCoupsPossibles(Liste,i,j,k,l);
          }
        }
      }
    }
  }
  return Liste;
}

void Liberer(struct CoupPossible *Liste){
  free(Liste);
}

void Jouer(struct Piece Plateau[NB][NB],uint8_t L1, uint8_t C1, uint8_t L2, uint8_t C2){
  Plateau[L2][C2].Type=Plateau[L1][C1].Type;
  Plateau[L2][C2].Couleur=Plateau[L1][C1].Couleur;
  Plateau[L1][C1].Type=AUCUNE;
  Plateau[L1][C1].Couleur=VIDE;
}

void JouerMeilleurCoup(struct Jeu *Jeu){
  struct CoupPossible* liste;
  long m=0;
  liste=CreerListeCoupsPossibles(Jeu->Plateau,Jeu->Joueur);
  struct CoupPossible* MeilleurCoup;
  struct CoupPossible* parcours;
  parcours = liste;
  //printf("liste = %p parcours =%p liste->Suivant =%p\n", liste, parcours, parcours->Suivant);
  while(parcours!=NULL){
    parcours->Evaluation=AlphaBetaAmi(Jeu->Plateau,gDonnees.Alpha,gDonnees.Beta,gDonnees.Profondeur,Jeu->Joueur);
    if(parcours->Evaluation>m){
      m=parcours->Evaluation;
      MeilleurCoup =parcours;
    }
    parcours=parcours->Suivant;
  }

  Jouer(Jeu->Plateau,MeilleurCoup->L1, MeilleurCoup->C1, MeilleurCoup->L2, MeilleurCoup->C2);
  if (gDonnees.JeuEnCours->Joueur==BLANC) ; gDonnees.JeuEnCours->Joueur==NOIR;
  if (gDonnees.JeuEnCours->Joueur==NOIR) ; gDonnees.JeuEnCours->Joueur==BLANC;

  //Jouer(Jeu->Plateau,liste->L1, liste->C1, liste->L2, liste->C2);
}

long AlphaBetaAmi(struct Piece Plateau[NB][NB], long Alpha, long Beta, uint8_t Profondeur, enum Couleur JoueurEnCours){
  struct Piece PlateauCopie[NB][NB];
  uint8_t i,j;
  if(Profondeur<=0){
    return Evaluer(Plateau,JoueurEnCours);
  }
  struct CoupPossible* liste;
  liste=CreerListeCoupsPossibles(Plateau,JoueurEnCours);
  struct CoupPossible* parcours;
  parcours = liste;
  while(parcours!=NULL){
    for(i=0;i<NB;i++){
      for(j=0;j<NB;j++){
        PlateauCopie[i][j]=Plateau[i][j];
      }
    }
    Jouer(PlateauCopie,parcours->L1,parcours->C1,parcours->L2,parcours->C2);
    Alpha=fmax(Alpha, AlphaBetaEnnemi(PlateauCopie, gDonnees.Alpha, gDonnees.Beta, gDonnees.Profondeur-1, JoueurEnCours));
    if(Alpha>=Beta){
      return Beta;
    }
    parcours=parcours->Suivant;
  }
  return Alpha;
}

long AlphaBetaEnnemi(struct Piece Plateau[NB][NB], long Alpha, long Beta, uint8_t Profondeur, enum Couleur JoueurEnCours){
  struct Piece PlateauCopie[NB][NB];
  uint8_t i,j;
  if(Profondeur<=0){
    return Evaluer(Plateau,JoueurEnCours);
  }
  struct CoupPossible* liste;
  liste=CreerListeCoupsPossibles(Plateau,JoueurEnCours);
  struct CoupPossible* parcours;
  parcours = liste;
  while(parcours!=NULL){
    for(i=0;i<NB;i++){
      for(j=0;j<NB;j++){
        PlateauCopie[i][j]=Plateau[i][j];
      }
    }
    Jouer(PlateauCopie,parcours->L1,parcours->C1,parcours->L2,parcours->C2);
    Beta=fmin(Beta, AlphaBetaAmi(PlateauCopie, gDonnees.Alpha, gDonnees.Beta, Profondeur-1, JoueurEnCours));
    if(Alpha>=Beta){
      return Alpha;
    }
    parcours=parcours->Suivant;

  }
  return Beta;
}
long Evaluer(struct Piece Plateau[NB][NB], enum Couleur JoueurEnCours){
  long s=0;
  int i,j;
  for(i=0;i<NB;i++){
    for(j=0;j<NB;j++){
      if(Plateau[i][j].Couleur==JoueurEnCours){
        if(Plateau[i][j].Type==ROI){
          s=s+1000;
        }
        if(Plateau[i][j].Type==REINE){
          s=s+50;
        }
        if(Plateau[i][j].Type==TOUR){
          s=s+25;
        }
        if(Plateau[i][j].Type==FOU){
          s=s+10;
        }
        if(Plateau[i][j].Type==CAVALIER){
          s=s+5;
        }
        if(Plateau[i][j].Type==PION){
          s=s+1;
        }
      }
    }
  }
  return s;
}

int NbCoupsPossibles(struct CoupPossible* Liste){
  struct CoupPossible* parcours;
  parcours = Liste;
  int i;
  while(parcours!=NULL){
    i=i+1;
    parcours=parcours->Suivant;
  }
  return i;
}

uint8_t FinDuJeu(struct Piece Plateau[NB][NB]){
  long s1,s2;
  s1=Evaluer(Plateau,BLANC);
  if (s1<1000){
    return 2;
  }
  s2=Evaluer(Plateau,NOIR);
  if (s2<1000){
    return 1;
  }
  return 0;
}

// Joue le fichier son passe en parametre, mp3, etc...
void JouerSon(char * FichierSon)
{
    // Musique de fond
    FMOD_SYSTEM      *system;
    FMOD_SOUND       *sound;
    FMOD_CHANNEL     *channel = 0;
    FMOD_RESULT       result;
    int               key;
    unsigned int      version;
    /*
        Create a System object and initialize.
    */
    result = FMOD_System_Create(&system);
    result = FMOD_System_GetVersion(system, &version);
    result = FMOD_System_Init(system, 32, FMOD_INIT_NORMAL, NULL);
    result = FMOD_System_CreateSound(system, FichierSon, FMOD_SOFTWARE, 0, &sound);
    result = FMOD_Sound_SetMode(sound, FMOD_LOOP_OFF);
    result = FMOD_System_PlaySound(system, FMOD_CHANNEL_FREE, sound, 0, &channel);
}

// Cette procedure permet une attente de x secondes, x peut etre en secondes mais aussi en flottant par exemple : 0.1 s
void Attente ( double Seconds )
{
    clock_t Endwait;
    Endwait = (int) (clock () + Seconds * CLOCKS_PER_SEC);
    while (clock() < Endwait);
}
