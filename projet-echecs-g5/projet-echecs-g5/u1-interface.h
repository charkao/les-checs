// u1-interface.h
// Sentinelle d'inclusion
#ifndef _u1_interface_h
#define _u1_interface_h

// Declarations externes - inclusion des fichiers d'entete
#include "main.h"
#include <FL/Fl_Double_Window.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Round_Button.H>
#include <FL/Fl_Value_Slider.H>
#include <FL/Fl_Value_Output.H>


// Definition des constantes
#define X_ZONE  10      // X de la zone
#define Y_ZONE  10      // Y de la zone
#define L_ZONE  800     // Largeur de la zone
#define H_ZONE  800     // Hauteur de la zone

// Declaration des objets de l'interface
struct Interface
{
    Fl_Double_Window*   Fenetre ;
    DrawingArea*        ZoneDessin ;
    Fl_Button*          BoutonQuitter ;
    Fl_Group*           GroupeBoutonsRadios;
    Fl_Round_Button*    BoutonRadio1;
    Fl_Round_Button*    BoutonRadio2;
    Fl_Round_Button*    BoutonRadio3;
    Fl_Value_Slider*    Curseur ;
    Fl_Button*          BoutonJouer;
    Fl_Button*          BoutonPasser;
    Fl_Button*          BoutonAnnuler;
    Fl_Value_Output*    ScoreB ;
    Fl_Value_Output*    ScoreN ;

} ;

// Declaration des objets de l'interface generale - ne pas supprimer
extern struct Interface gInterface ;


// Declaration des sous-programmes
void CreerInterface();
void ActualiserInterface();

#endif // _u1_interface_h
