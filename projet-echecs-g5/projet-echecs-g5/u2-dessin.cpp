// u2-dessin.cpp
// Declarations externes - inclusion des fichiers d'entete
// Librairies standards
#include <iostream>         // cin, cout, ...
// Librairies fltk
#include <FL/Fl.H>
#include <FL/fl_draw.H>     // fonctions de dessin
// Programmes locaux
#include "u1-interface.h"
#include "u2-dessin.h"
#include "u4-fonctions.h"
#include <FL/Fl_GIF_Image.H>
#include <FL/Fl_JPEG_Image.H>

// Declaration pour utiliser iostream
using namespace std;

// DessinerZone
void ZoneDessinDessinerCB( Fl_Widget* widget, void* data )
{   int i, j;
    //Lignes paires cases blanches
    for (j=0; j<4; j++){
      for (i=0; i<4; i++){
        fl_color(FL_WHITE);
        fl_rectf(X_ZONE+2*j*100, Y_ZONE+2*i*100, 100, 100);
      }
    }
    //Lignes impaires cases blanches
    for (j=0; j<4; j++){
      for (i=0; i<4; i++){
        fl_color(FL_WHITE);
        fl_rectf(X_ZONE+(2*j+1)*100, Y_ZONE+(2*i+1)*100, 100, 100);
      }
    }
    //Lignes paires cases noires
    for (j=0; j<4; j++){
      for (i=0; i<4; i++){
        fl_color(FL_DARK_BLUE);
        fl_rectf(X_ZONE+2*j*100, Y_ZONE+(2*i+1)*100, 100, 100);
      }
    }
    //Lignes impaires cases noires
    for (j=0; j<4; j++){
      for (i=0; i<4; i++){
        fl_color(FL_DARK_BLUE);
        fl_rectf(X_ZONE+(2*j+1)*100, Y_ZONE+2*i*100, 100, 100);
      }
    }
// Placement des pieces sur le plateau en fontion de JeuEnCours
for (i=0; i<8; i++){
  for (j=0; j<8; j++){
    if(gDonnees.JeuEnCours->Plateau[i][j].Couleur==BLANC){
        if (gDonnees.JeuEnCours->Plateau[i][j].Type==TOUR){
            Fl_GIF_Image ImageTourB("media/tour-blanc-64x64.gif") ;
            ImageTourB.draw(X_ZONE+j*100+20, Y_ZONE+i*100+20);
        }
        if (gDonnees.JeuEnCours->Plateau[i][j].Type==CAVALIER){
            Fl_GIF_Image ImageCavalierB("media/cavalier-blanc-64x64.gif") ;
            ImageCavalierB.draw(X_ZONE+j*100+20, Y_ZONE+i*100+20);
        }
        if (gDonnees.JeuEnCours->Plateau[i][j].Type==FOU){
            Fl_GIF_Image ImageFouB("media/fou-blanc-64x64.gif") ;
            ImageFouB.draw(X_ZONE+j*100+20, Y_ZONE+i*100+20);
        }
        if (gDonnees.JeuEnCours->Plateau[i][j].Type==REINE){
            Fl_GIF_Image ImageReineB("media/reine-blanc-64x64.gif") ;
            ImageReineB.draw(X_ZONE+j*100+20, Y_ZONE+i*100+20);
        }
        if (gDonnees.JeuEnCours->Plateau[i][j].Type==ROI){
            Fl_GIF_Image ImageRoiB("media/roi-blanc-64x64.gif") ;
            ImageRoiB.draw(X_ZONE+j*100+20, Y_ZONE+i*100+20);
        }
        if (gDonnees.JeuEnCours->Plateau[i][j].Type==PION){
            Fl_GIF_Image ImagePionB("media/pion-blanc-64x64.gif") ;
            ImagePionB.draw(X_ZONE+j*100+20, Y_ZONE+i*100+20);
        }
    }
    if(gDonnees.JeuEnCours->Plateau[i][j].Couleur==NOIR){
        if (gDonnees.JeuEnCours->Plateau[i][j].Type==TOUR){
            Fl_GIF_Image ImageTourN("media/tour-noir-64x64.gif") ;
            ImageTourN.draw(X_ZONE+j*100+20, Y_ZONE+i*100+20);
        }
        if (gDonnees.JeuEnCours->Plateau[i][j].Type==CAVALIER){
            Fl_GIF_Image ImageCavalierN("media/cavalier-noir-64x64.gif") ;
            ImageCavalierN.draw(X_ZONE+j*100+20, Y_ZONE+i*100+20);
        }
        if (gDonnees.JeuEnCours->Plateau[i][j].Type==FOU){
            Fl_GIF_Image ImageFouN("media/fou-noir-64x64.gif") ;
            ImageFouN.draw(X_ZONE+j*100+20, Y_ZONE+i*100+20);
        }
        if (gDonnees.JeuEnCours->Plateau[i][j].Type==REINE){
            Fl_GIF_Image ImageReineN("media/reine-noir-64x64.gif") ;
            ImageReineN.draw(X_ZONE+j*100+20, Y_ZONE+i*100+20);
        }
        if (gDonnees.JeuEnCours->Plateau[i][j].Type==ROI){
            Fl_GIF_Image ImageRoiN("media/roi-noir-64x64.gif") ;
            ImageRoiN.draw(X_ZONE+j*100+20, Y_ZONE+i*100+20);
        }
        if (gDonnees.JeuEnCours->Plateau[i][j].Type==PION){
            Fl_GIF_Image ImagePionN("media/pion-noir-64x64.gif") ;
            ImagePionN.draw(X_ZONE+j*100+20, Y_ZONE+i*100+20);
        }
    }
}}}
