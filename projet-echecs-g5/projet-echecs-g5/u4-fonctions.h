/* Othello u4_fonctions.h - proposition initiale de structure de donnees et de fonctions (P. Kocelniak)*/
/* NOTA BENE : A adapter pour les echecs */

// u4-fonctions.h
// Sentinelle d'inclusion
#ifndef _u4_fonctions_h
#define _u4_fonctions_h

#include <stdint.h> // uint8_t, ...
#include <math.h>

// Definition des constantes
#define DUREE_CYCLE 10    // 0.500 secondes, depend du materiel utilise

#define NB 8 // Nombre de lignes et de colonnes du plateau d'échecs, invariable
// Declaration des donnees du projet

/*
Structures de donnees du projet
*/
/* L'enumeration Couleur sert a la fois :
   - pour designer le type de chaque cellule du plateau de jeu
   - pour la couleur du joueur en cours (2 valeurs possibles dans ce cas : BLANC ou NOIR
*/
enum TypePiece {AUCUNE, PION, TOUR, CAVALIER, FOU, REINE, ROI};
enum Couleur {VIDE, BLANC, NOIR}; // VIDE : 0, BLANC : 1, NOIR : 2

// Une piece du plateau = un type de piece et une couleur de piece
struct Piece{
enum TypePiece Type;
enum Couleur Couleur;
};

// Situation de jeu
struct Jeu
{
    enum Couleur Joueur; // Couleur du joueur en cours
    struct Piece Plateau[NB][NB]; // Plateau de jeu alloue automatiquement car le nombre de lignes et colonnes est invariable aux echecs
};

// Structure pour la liste des coups possibles (liste chainee simple)
struct CoupPossible
{
// uint8_t utilise pour reduire le plus possible la place occupee en memoire
uint8_t L1; // Ligne de la cellule jouee, depart
uint8_t C1; // Colonne de la cellule jouee, depart
uint8_t L2; // Ligne de la cellule jouee, arrivee
uint8_t C2; // Colonne de la cellule jouee, arrivee
long Evaluation; // Evaluation du coup, vue du point de vue du Joueur de JeuEnCours
struct CoupPossible *Suivant; // Pointeur vers le coup possible suivant
};

// Structure globale pour les variables fonctionnelles
struct Donnees
{
    // Variables du projet a definir ici
    struct Jeu *JeuEnCours; // Structure de jeu courante, pointeur afin de faire une liste d'historique des coups joues en fonction avancee
    bool EstHumainBlanc; // Ordi ou humain pour le joueur 1 Blanc
    bool EstHumainNoir; // Ordi ou humain pour le joueur 2 Noir
    uint8_t Profondeur; // Profondeur algorithme AlphaBeta
    uint8_t NbClics;     // Mode joueur Humain : Nb de clics effectue, 0, 1 (L1,C1 cellule jouee, depart), 2 (L2, C2, cellule jouee, arrivee)
    uint8_t ClicL1, ClicC1; // Mode Joueur Humain : Coordonnees clic 1, depart
    uint8_t ClicL2, ClicC2; // Mode Joueur Humain : Coordonnees clic 2, arrivee
    long Alpha;
    long Beta;
};

// Variable globale, gDonnees est accessible depuis toutes les fonctions de tous les modules, u1... a u4...
extern struct Donnees gDonnees;

/*
Prototypes des fonctions a creer dans u4-fonctions.cpp
*/
/* Initialisation des donnees initiales : jeu, EstHumainBlanc, EstHumainNoir, Profondeur, NbClics, ClicL1, ClicC1, ClicL2, ClicC2,... */
void InitialiserDonnees();

/*
Gestion de la liste des coups possibles. Choix d'une liste chainee simple.
*/

/* Cette fonction retourne true si le coup joue de L1,C1 en L2,C2 est valable, false sinon */
bool CoupJouable(struct Piece Plateau[NB][NB], uint8_t L1, uint8_t C1, uint8_t L2, uint8_t C2, enum Couleur Joueur);

/* Initialisation de la liste des coups possibles :
   retourne un pointeur sur une liste initiale constituee d'une seule premiere cellule de type struct CoupPossible
   Pour cette premiere cellule "vide":
   - L1, C1, L2, C2 seront initialises avec la valeur -1 signifiant que la cellule est vide pour le moment
   - Evaluation peut etre initialise a 0
   - Le pointeur Suivant pointera sur le pointeur NULL
*/
struct CoupPossible* CreerListesCoupsPossiblesInitiale();

/* Cette fonction insere une nouvelle cellule a la fin de la liste des coups possibles Liste avec les valeurs des parametres L1, C1, L2, C2 */
void AjouterListeCoupsPossibles(struct CoupPossible* Liste, uint8_t L1, uint8_t C1, uint8_t L2, uint8_t C2);

/* Cette fonction cree la liste complete des coups possibles pour le Plateau passe en parametre et le joueur en cours
   Principes :
   - Appel a CreerListesCoupsPossiblesInitiale
   - Methode "force brute" : on teste via des boucles tous les coups L1, C1 , L2, C2 dans le Plateau
   - Pour chaque coup, la fonction CoupJouable est utilisee
   - Si CoupJouable retourne true, on ajoute a la liste des coups possibles via la fonction AjouterListeCoupsPossibles
*/
struct CoupPossible* CreerListeCoupsPossibles(struct Piece Plateau[NB][NB], enum Couleur Joueur);

/* Liberation de toute la memoire allouee dynamiquement pour la liste Liste */
void Liberer(struct CoupPossible *Liste);

/*
Jeu ordinateur
*/

/* Cette fonction joue le coup L1, C1, L2, C2 dans Plateau et modifie donc les valeurs des cellules de Plateau */
void Jouer(struct Piece Plateau[NB][NB], uint8_t L1, uint8_t C1, uint8_t L2, uint8_t C2);

/* Cette fonction permet de faire jouer l'ordinateur
   Cette fonction cree la liste des coups possibles pour la situation de jeu passee en parametres
   Elle cherche le meilleur coup a jouer et le joue finalement en modifiant les donnees du jeu courant :
   - Appel a CreerListeCoupsPossibles
   - Parcours de la liste des coups possibles et calcul de Evaluation pour chaque coup via un appel a AlphaBetaAmi
   - Choix du meilleur coup (Evaluation la plus grande)
   - On joue le meilleur coup et on change les donnees du jeu pour passer au joueur suivant
*/
void JouerMeilleurCoup(struct Jeu *Jeu);

/*
Fonctions AlphaBeta : mise en oeuvre de l'algorithme AlphaBeta pour retourner l'Evaluation d'un coup de la liste des coups possibles
Cf. fiche AlphaBeta et pseudo code fourni
Sorte de recursivite avec 2 fonctions :
- AlphaBetaAmi fait des appels a AlphaBetaEnnemi
- AlphaBetaEnnemi fait des appels a AlphaBetaAmi
*/
long AlphaBetaAmi(struct Piece Plateau[NB][NB], long Alpha, long Beta, uint8_t Profondeur, enum Couleur JoueurEnCours);
long AlphaBetaEnnemi(struct Piece Plateau[NB][NB], long Alpha, long Beta, uint8_t Profondeur, enum Couleur JoueurEnCours);

/* Retourne l'evaluation du plateau de jeu Plateau vue du joueur en cours */
long Evaluer(struct Piece Plateau[NB][NB], enum Couleur JoueurEnCours);

/*
Autres fonctions utilitaires
*/

/* Retourne le nombre de coups possibles de la liste */
int NbCoupsPossibles(struct CoupPossible* Liste);

/* Conditions de fin du jeu. Retourne 0 si le jeu n'est pas termine, 1 si le jeu est termine et les blancs ont gagne, 2 si le jeu est termine et les noirs ont gagne */
uint8_t FinDuJeu(struct Piece Plateau[NB][NB]);

/* Fonctions facultatives de trace dans un fichier en mode texte : cela pourra servir a des verifications si vous avez des problemes dans vos fonctions precedentes */
FILE* OuvrirFichierTrace(char *Fichier); // Ouverture en mode texte et ecriture d'un fichier de trace dont le nom est passe en parametre
void TraceExporterListeCoupsPossibles (FILE *f, struct CoupPossible* Liste); // Ecrire dans le flux de fichier f la liste des coups possibles
void TraceExporterDonnees (FILE *f, struct Donnees *Donnees); // Ecrire dans le flux de fichier f les differentes variables de la structure Donnees
void FermerFichierTrace(FILE *f); // Fermeture du fichier de trace

// Utilitaires fournis par le kitc-phelma
void JouerSon(char *) ;         // Jouer un son
void Attente(double Seconds);   // Procedure d'attente

#endif // _u4_fonctions_h
