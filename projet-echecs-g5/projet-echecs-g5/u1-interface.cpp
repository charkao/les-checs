// u1-interface.cpp
// Declarations externes - inclusion des fichiers d'entete
// Librairies standards
#include <iostream>         // cin, cout, ...
// Programmes locaux
#include "u1-interface.h"
#include "u2-dessin.h"
#include "u3-callbacks.h"
#include "u4-fonctions.h"

// Declaration pour utiliser iostream
using namespace std;

// Definition des donnees de l'interface - structure globale de variables
struct Interface gInterface ;

// CreerInterface
void CreerInterface()
{
    // Creation de la fenetre principale
    gInterface.Fenetre = new Fl_Double_Window(1200,900);
    gInterface.Fenetre->label("Projet Type") ;
    gInterface.Fenetre->begin() ;

    // Creation de la zone de dessin
    gInterface.ZoneDessin = new DrawingArea(X_ZONE,Y_ZONE,L_ZONE,H_ZONE);
    gInterface.ZoneDessin->draw_callback( ZoneDessinDessinerCB, NULL ) ;
    gInterface.ZoneDessin->mouse_callback( ZoneDessinSourisCB, NULL ) ;
    gInterface.ZoneDessin->keyboard_callback( ZoneDessinClavierCB, NULL ) ;

    // Creation du bouton Quitter
    gInterface.BoutonQuitter = new Fl_Button(20, 840, 100, 20, "Quitter") ;
    gInterface.BoutonQuitter->callback( BoutonQuitterCB, NULL ) ;

    // Creation du bouton Jouer
    gInterface.BoutonJouer = new Fl_Button(960, 230, 100, 20, "Jouer") ;
    gInterface.BoutonJouer->callback( BoutonJouerCB, NULL ) ;

    // Creation du bouton Passer
    gInterface.BoutonPasser = new Fl_Button(960, 270, 100, 20, "Passer") ;
    gInterface.BoutonPasser->callback( BoutonPasserCB, NULL ) ;

    // Creation du bouton Quitter
    gInterface.BoutonAnnuler = new Fl_Button(960, 310, 100, 20, "Annuler Coup") ;
    gInterface.BoutonAnnuler->callback( BoutonAnnulerCB, NULL ) ;


    // Creation du bouton radio
    gInterface.GroupeBoutonsRadios = new Fl_Group(870, 80, 100, 20, "Mode de jeu") ;
    gInterface.GroupeBoutonsRadios->begin() ;
    gInterface.BoutonRadio1 = new Fl_Round_Button(970, 60, 100, 20, "Ordinateur vs Ordinateur") ;
    gInterface.BoutonRadio1->type(FL_RADIO_BUTTON);
    gInterface.BoutonRadio1->callback(BoutonRadioCB, NULL ) ;
    gInterface.BoutonRadio2 = new Fl_Round_Button(970, 80, 100, 20, "Ordinateur vs Humain") ;
    gInterface.BoutonRadio2->type(FL_RADIO_BUTTON);
    gInterface.BoutonRadio2->callback(BoutonRadioCB, NULL ) ;
    gInterface.BoutonRadio3 = new Fl_Round_Button(970, 40, 100, 20, "Humain vs Humain") ;
    gInterface.BoutonRadio3->type(FL_RADIO_BUTTON);
    gInterface.BoutonRadio3->callback(BoutonRadioCB, NULL ) ;
    gInterface.GroupeBoutonsRadios->end() ;

    // Creation du bouton curseur
    gInterface.Curseur = new Fl_Value_Slider( 1010, 130, 100, 20, "Niveau de l'ordinateur" ) ;
    gInterface.Curseur->type( FL_HORIZONTAL ) ;
    gInterface.Curseur->align( FL_ALIGN_LEFT ) ;
    gInterface.Curseur->callback( CurseurCB, NULL ) ;
    gInterface.Curseur->bounds( 1,10 ) ;
    gInterface.Curseur->precision( 0 ) ;

    gInterface.ScoreB = new Fl_Value_Output(960, 370, 100, 20, "Score joueur blanc") ;
    gInterface.ScoreN = new Fl_Value_Output(960, 410, 100, 20, "Score joueur noir") ;

    // Affichage de la fenetre
    gInterface.Fenetre->end();
    gInterface.Fenetre->show();

}

void ActualiserInterface()
{

}
