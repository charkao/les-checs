// u3-callbacks.h
// Sentinelle d'inclusion
#ifndef _u3_callbacks_h
#define _u3_callbacks_h

// Declarations externes
#include <FL/Fl_Widget.H>

// Declaration des sous-programmes
void TraiterCycleCB() ;  //MAGUELONE
void ZoneDessinSourisCB( Fl_Widget* widget, void* data ) ;  //MAGUELONE
void ZoneDessinClavierCB( Fl_Widget* widget, void* data ) ;  //MAGUELONE
void BoutonQuitterCB( Fl_Widget* w, void* data ) ;  //MAGUELONE
void BoutonRadioCB(Fl_Widget* w, void* data);  //MAGUELONE
void CurseurCB ( Fl_Widget* w, void* data ) ;  //MAGUELONE
void BoutonJouerCB ( Fl_Widget* w, void* data );  //MAGUELONE
void BoutonPauseCB ( Fl_Widget* w, void* data );  //MAGUELONE
void BoutonAnnulerCB ( Fl_Widget* w, void* data );  //MAGUELONE



#endif // _u3_callbacks_h
