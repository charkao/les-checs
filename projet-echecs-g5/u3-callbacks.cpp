
// u3-callbacks.cpp
// Declarations externes - inclusion des fichiers d'entete
// Librairies standards
#include <iostream>     // cout, cin, ...
// Librairies fltk
#include <FL/Fl.H>
#include <FL/fl_ask.H>  // fl_message, fl_alert, fl_ask
#include <FL/Fl_File_Chooser.H> // fl_file_chooser
// Programmes locaux
#include "u1-interface.h"
#include "u3-callbacks.h"
#include "u4-fonctions.h"

// Declaration pour utiliser iostream
using namespace std;

// TraiterCycle
void TraiterCycleCB()
{

    // Trace pour bien montrer que la fonction est appelee cycliquement
    // printf(""Appel de TraiterCycleCB");

    // Traitements cycliques a placer ici :

    int res;
    res=FinDuJeu(gDonnees.JeuEnCours->Plateau);
    if (res==1) {
      printf("Les blancs ont gagnÃ©");
      exit(0);
    }
    if (res==2) {
      printf("Les noirs ont gagnÃ©");
      exit(0);
    }
    if (gDonnees.Start==1){
    if ((gDonnees.EstHumainBlanc)&&(gDonnees.EstHumainNoir)){
      if (gDonnees.JeuEnCours->Joueur==BLANC){
          if (gDonnees.NbClics==2){
                Jouer(gDonnees.JeuEnCours->Plateau, gDonnees.ClicL1, gDonnees.ClicC1, gDonnees.ClicL2, gDonnees.ClicC2);
                gDonnees.NbClics=0;
                gDonnees.JeuEnCours->Joueur=NOIR;
          }
      }
      if (gDonnees.JeuEnCours->Joueur==NOIR){
          if (gDonnees.NbClics==2){
                Jouer(gDonnees.JeuEnCours->Plateau, gDonnees.ClicL1, gDonnees.ClicC1, gDonnees.ClicL2, gDonnees.ClicC2);
                gDonnees.NbClics=0;
                gDonnees.JeuEnCours->Joueur=BLANC;
          }
      }
    }

    if ((gDonnees.EstHumainBlanc)&&(gDonnees.EstHumainNoir==false)){
        if (gDonnees.JeuEnCours->Joueur==BLANC){
            if (gDonnees.NbClics==2){
                  Jouer(gDonnees.JeuEnCours->Plateau, gDonnees.ClicL1, gDonnees.ClicC1, gDonnees.ClicL2, gDonnees.ClicC2);
                  gDonnees.NbClics=0;
                  gDonnees.JeuEnCours->Joueur=NOIR;gInterface.Curseur->callback( CurseurCB, NULL ) ;
            }
        }
        else JouerMeilleurCoup(gDonnees.JeuEnCours);
    }

    if ((gDonnees.EstHumainNoir)&&(gDonnees.EstHumainBlanc==false)){
        if (gDonnees.JeuEnCours->Joueur==NOIR){
            if (gDonnees.NbClics==2){
                  Jouer(gDonnees.JeuEnCours->Plateau, gDonnees.ClicL1, gDonnees.ClicC1, gDonnees.ClicL2, gDonnees.ClicC2);
                  gDonnees.NbClics=0;
                  gDonnees.JeuEnCours->Joueur=BLANC;
            }
        }
        else JouerMeilleurCoup(gDonnees.JeuEnCours);gInterface.Curseur->callback( CurseurCB, NULL ) ;
    }
    if ((gDonnees.EstHumainBlanc==false)&&(gDonnees.EstHumainNoir==false)){
        JouerMeilleurCoup(gDonnees.JeuEnCours);
    }

    // On redessine la zone
    gInterface.ZoneDessin->redraw() ;

    /* Correctif PK */
    // On reactualise les donnees de l'interface
    ActualiserInterface();

    // Code a activer en cas de probleme pour saisir les evenements du clavier
    // Probleme : si les evenements du clavier ne sont pas bien pris en compte pour agir sur la zone de dessin.
    // Solution : On ramene systematiquement le focus des evenements sur la zone de dessin
    // Attention, cela peut perturber certains elements d'interface comme des champs de saisie    if (gDonnees.Start==1){

    // Fl::focus(gInterface.ZoneDessin);

    // Fin code a activer en cas de probleme
}
}

// ZoneDessinSourisCB
void ZoneDessinSourisCB(Fl_Widget* widget, void* data )
{
    // ATTENTION : X et Y ne sont pas relatifs a la zone mais a la fenetre qui la contient !!!!

    // Exemple d'evenement : clic souris
    if ( Fl::event() == FL_PUSH ){gInterface.Curseur->callback( CurseurCB, NULL ) ;
        printf("Mouse push = %i x = %i y = %i\n", Fl::event_button(), Fl::event_x(), Fl::event_y());
        if(gDonnees.NbClics ==0){
            if ((Fl::event_x()>X_ZONE)&&(Fl::event_x()<X_ZONE+L_ZONE)){
                if ((Fl::event_y()>Y_ZONE)&&(Fl::event_y()<Y_ZONE+H_ZONE)){
                    gDonnees.ClicC1=(Fl::event_x()-X_ZONE)/100;
                    gDonnees.ClicL1=(Fl::event_y()-Y_ZONE)/100;
                    gDonnees.NbClics=1;
                }
            }
        }

    else{
        if ((Fl::event_x()>X_ZONE)&&(Fl::event_x()<X_ZONE+L_ZONE)){
            if ((Fl::event_y()>Y_ZONE)&&(Fl::event_y()<Y_ZONE+H_ZONE)){gInterface.Curseur->callback( CurseurCB, NULL ) ;
                gDonnees.ClicC2=(Fl::event_x()-X_ZONE)/100;
                gDonnees.ClicL2=(Fl::event_y()-Y_ZONE)/100;
            }
        }
        if (CoupJouable(gDonnees.JeuEnCours->Plateau, gDonnees.ClicL1, gDonnees.ClicC1,  gDonnees.ClicL2, gDonnees.ClicC2, gDonnees.JeuEnCours->Joueur)==true){
            gDonnees.NbClics=2;
        }
        else gDonnees.NbClics=0;
    }
}
}


// ZoneDessinClavierCB
void ZoneDessinClavierCB( Fl_Widget* widget, void* data )
{
    // Definition des variables
    int Touche ;

    // Recuperation de la touche clavier activee
    Touche = Fl::event_key() ;

    // Traitement de la touche
    switch ( Touche )
    {
        // Touches speciales
        case FL_Left :
            printf("Appui sur la touche Gauche\n");
            break;
        case FL_Right :
            printf("Appui sur la touche Droite\n");
            break;gInterface.Curseur->callback( CurseurCB, NULL ) ;
        case FL_Up :
            printf("Appui sur la touche Haut\n");
            break ;
        case FL_Down :
            printf("Appui sur la touche Bas\n");
            break ;
        // Caracteres
        case ' ' :
            printf("Appui sur la touche Espace\n");
            break ;
        case 'a' :
            printf("Appui sur le caractere a\n");
            break ;
        case 'b' :
            printf("Appui sur le caractere b\n");
            break ;
        case '1' :
            printf("Appui sur le caractere 1\n");
            break ;
        case '2' :
            printf("Appui sur le chiffre 2\n");
            break ;
    }
}

void BoutonQuitterCB(Fl_Widget* w, void* data)
{
    // Fin du programme
    exit(0) ;
}


void BoutonRadioCB(Fl_Widget* w, void* data)
{
    /* Correctif PK */
    // On regarde quel mode joueur est selectionne
    if ( gInterface.BoutonRadio1->value() == 1 ) {
        gDonnees.EstHumainBlanc = false ;
        gDonnees.EstHumainNoir = false ;
    }
    else  if ( gInterface.BoutonRadio2->value() == 1 ) {
        gDonnees.EstHumainBlanc = false ;
        gDonnees.EstHumainNoir = true ;
    }
    else  if ( gInterface.BoutonRadio3->value() == 1 ) {
        gDonnees.EstHumainBlanc = true ;
        gDonnees.EstHumainNoir = true ;
    }
}

void CurseurCB ( Fl_Widget* w, void* data )
{
  gDonnees.Profondeur=gInterface.Curseur->value();
}

void BoutonJouerCB ( Fl_Widget* w, void* data )
{   gDonnees.Start=1;
}

void BoutonPauseCB ( Fl_Widget* w, void* data )
{   gDonnees.Start=0;
}

void BoutonAnnulerCB ( Fl_Widget* w, void* data )
{
}
